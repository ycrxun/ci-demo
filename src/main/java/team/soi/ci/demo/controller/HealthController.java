package team.soi.ci.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Soi at 2017/1/18.
 * @description
 */
@RestController
@RequestMapping("/health")
public class HealthController {

    @GetMapping
    public Object health() {
        Map<String, Object> map = new HashMap<>();
        map.put("appName", "ci-demo");
        map.put("appVersion", "0.0.1");
        map.put("os", System.getenv("os.name"));
        map.put("time", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        return map;
    }
}
