package team.soi.ci.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Soi at 2017/1/18.
 * @description
 */
@Controller
@RequestMapping("/")
public class HomeController {

    @GetMapping
    public Object index(){
        return "redirect:/health";
    }
}
