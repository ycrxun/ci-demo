FROM tomcat:alpine

COPY ./target/ci-demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/ROOT.war

EXPOSE 8080